<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class ContaoPortalEngineFlexibleList extends ContentElement
{
	
	
	protected $strTemplate = 'cpece_flexiblelist';
	protected $zahnaerzte;
	
	public function generate()
	{
		
		
		$sqla = "SELECT * from tl_cpe_zahnaerzte za
		
				inner join tl_cpe_seourls_zahnaerzte as zasu on za.id = zasu.idZahnaerzte	
					
				where ort = ? order by premium desc ";
		
		$this->zahnaerzte = $this->Database->prepare($sqla)->limit($this->cpefl_field3)->execute($this->cpefl_field1);
		
		
		
		return parent::generate();
	}
	
	public function compile()
	{
		$this->Template->rs= $this->zahnaerzte;
		$this->Template->modulordner = $GLOBALS['CPE-Conf']->URL_Modulordner."/";
	}
	
}
