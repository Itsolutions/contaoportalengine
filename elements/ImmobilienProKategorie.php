<?php 

namespace ContaoPortalEngine;

class ImmobilienProKategorie extends \ContentElement
{
	
	protected $strTemplate = 'cpece_immobilienProKategorie';
        
	public function generate() {

            if (TL_MODE == 'BE') {
                $objTemplate = new \BackendTemplate('be_wildcard');

                $objTemplate->wildcard = '### Contao-Portal-Engine - ImmobilienProKategorie ###';
                $objTemplate->wildcard.= '<br/> Kategorie        : '. $this->getKategoriename($this->cpe_kategorie);	

                return $objTemplate->parse();
            }

            switch ($GLOBALS['CPE-TARGET']) {
                case "cpe-detail":
                    $this->generateCPEDetail();
                    $this->strTemplate = 'cpece_immobilienDetails';
                    break;
                default:$this->generateCPEList();
            }
            
            return parent::generate();
	}

        public function generateCPEList() {

            $category_filter = array();

            $this->cpe_kategorie > 1    ?   $category_filter[] = $this->cpe_kategorie : "";
            $this->cpe_kategorie2 > 1   ?   $category_filter[] = $this->cpe_kategorie2 : "";
            $this->cpe_kategorie3 > 1   ?   $category_filter[] = $this->cpe_kategorie3 : "";
            $this->cpe_kategorie4 > 1   ?   $category_filter[] = $this->cpe_kategorie4 : "";


            $where = array();
            
            if (!empty($category_filter) && is_array($category_filter)) {
                foreach ($category_filter as $key => $value) {
                    $where[] = "kategorie = " . $value;
                }
            }

            $order = " order by preis desc";

            $sqla = "SELECT * from tl_cpe_immobilien za ".
                "inner join tl_cpe_seourls_immobilien as zasu on za.id = zasu.idImmobilien";


              //  echo $sqla; exit();



            if (!empty($where))
                $query = sprintf($sqla. ' WHERE %s', implode(' AND ', $where));
            else
                $query = $sqla;



            $this->rs= $this->Database->prepare($query.$order)->execute();
            

        }
        
        public function generateCPEDetail() {
            
             $this->rs= $this->Database->prepare("SELECT * from tl_cpe_immobilien za
                    inner join tl_cpe_seourls_immobilien as zasu on za.id = zasu.idImmobilien where za.id = ?")
                             ->execute($GLOBALS['CPE-ID']);           
        }
        
	   public function compile() {
            
            $this->Template->rs = $this->rs;


            strlen($this->cpe_title) > 0 ? $this->Template->topic = $this->cpe_title : $this->getKategoriename($this->cpe_kategorie);
       
            $this->Template->fBezeichnung = "bezeichnung_".$GLOBALS['TL_LANGUAGE'];
            $this->Template->fBezeichnung2 = "bezeichnung2_".$GLOBALS['TL_LANGUAGE'];
            $this->Template->modulordner = $GLOBALS['CPE-Conf']->URL_Modulordner."/";
	}
        
        public function getKategoriename($kategorie_id) {
            
            $katname = "name_{$GLOBALS['TL_LANGUAGE']}";
            return $this->Database->prepare("SELECT name_{$GLOBALS['TL_LANGUAGE']} from tl_cpe_kategorien where id =?")->execute($kategorie_id)->$katname;
        }
	
}