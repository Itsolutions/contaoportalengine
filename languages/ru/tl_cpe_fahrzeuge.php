<?php

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['edit'] = array("edit","edit");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['copy'] = array("copy","copy");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['delete'] = array("delete","delete");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['show'] = array("show","show");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['fahrzeug_id'] = array("Номер ТС");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['kategorie'] = array("Категория");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['hersteller'] = array("Производитель");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['fahrzeugart'] = array("Новое/БУ/На заказ");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['anzahl_achsen'] =array ("Количество осей");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['erstzulassung'] = array ("Год выпуска");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['bereifung'] =array ("Резина");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['thumbnail'] =array ("Thumbnail");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['image'] =array ("Image");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['gesamtgewicht'] =array ("Общая грузоподьемность");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['achsaggregatlast'] =array ("Нагрузка на оси");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['sattellast'] =array ("Нагрузка на гузнек");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['leergewicht'] =array ("Собственный вес");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['nutzlast'] =array ("Чистая грузоподьемностьx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['gesamtlaenge'] =array ("Общая длина");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['aufsattelhoehe'] =array ("Высота до шкворня");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['ladehoehe'] =array ("Высота полуприцепа");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['laenge_schwanenhals'] =array ("gooseneck length appro");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['laenge_ladebett'] =array ("Длина погрузочной площадки");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['breite'] =array ("Ширина");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['teleskopierbar_um'] =array ("extendable to approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['verbreiterbar_auf'] =array ("outriggers to approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['feste_auflage'] =array ("part behind solid gooseneck approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['achsabstand'] =array ("wheelbase approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['sattelvormass'] =array ("king pin front-end approx");



$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['durchschwenkradius_hinten'] =array ("Радиус до тягача");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['freitext_de'] =array ("Freitext [DE]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['freitext_en'] =array ("Freitext [EN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['freitext_ru'] =array ("Freitext [RU]");





?>