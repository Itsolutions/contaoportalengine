<?php

$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['edit'] = array("bearbeiten","bearbeiten");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['copy'] = array("kopieren","kopieren");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['delete'] = array("loeschen","loeschen");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['show'] = array("show","show");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['objektnummer'] = array("Objektnummer");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_de'] = array("Bezeichnung [DE]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_de'] = array("Bezeichnung2 [DE]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_en'] = array("Bezeichnung [EN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_en'] = array("Bezeichnung2 [EN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_cn'] = array("Bezeichnung [CN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_cn'] = array("Bezeichnung2 [CN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['ort'] = array("Ort");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['flaeche'] = array("Fläche in qm");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['grundstueckflaeche'] = array("Grundstückfläche in qm");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['preis'] = array("Preis");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['preisgebot'] = array("Preis als Gebot?");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['preisgebottext'] = array("Kaufpreis gegen Gebot!");



$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['mieteinnahmen'] = array("Mieteinnahmen");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['asset'] = array("Asset");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['kategorie'] = array("Kategorie");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['thumbnail'] =array ("Thumbnail");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image1'] =array ("Bild 1");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image2'] =array ("Bild 2");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image3'] =array ("Bild 3");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image4'] =array ("Bild 4");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image5'] =array ("Bild 5");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image6'] =array ("Bild 6");



$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_de'] =array ("Freitext [DE]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_en'] =array ("Freitext [EN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_ru'] =array ("Freitext [CN]");


?>