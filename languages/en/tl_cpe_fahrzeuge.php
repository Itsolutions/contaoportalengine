<?php

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['edit'] = array("edit","edit");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['copy'] = array("copy","copy");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['delete'] = array("delete","delete");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['show'] = array("show","show");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['fahrzeug_id'] = array("Vehicle ID");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['kategorie'] = array("Category");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['hersteller'] = array("Manufacturer");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['fahrzeugart'] = array("Vehicle Variant");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['anzahl_achsen'] =array ("Number of axles");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['erstzulassung'] = array ("First approval");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['bereifung'] =array ("Tire (s)");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['thumbnail'] =array ("Thumbnail");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['image'] =array ("Image");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['gesamtgewicht'] =array ("GW");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['achsaggregatlast'] =array ("axle load");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['sattellast'] =array ("5th wheel load");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['leergewicht'] =array ("dead weight approx");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['nutzlast'] =array ("payload approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['gesamtlaenge'] =array ("total length approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['aufsattelhoehe'] =array ("5th wheel height approx");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['ladehoehe'] =array ("loading height approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['laenge_schwanenhals'] =array ("gooseneck length appro");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['laenge_ladebett'] =array ("loading bed length approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['breite'] =array ("width approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['teleskopierbar_um'] =array ("extendable to approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['verbreiterbar_auf'] =array ("outriggers to approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['feste_auflage'] =array ("part behind solid gooseneck approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['achsabstand'] =array ("wheelbase approx");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['sattelvormass'] =array ("king pin front-end approx");



$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['durchschwenkradius_hinten'] =array ("gooseneck swing (rear)");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['freitext_de'] =array ("Freitext [DE]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['freitext_en'] =array ("Freitext [EN]");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_fahrzeuge']['freitext_ru'] =array ("Freitext [RU]");





?>