<?php

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['edit'] = array("bearbeiten","bearbeiten");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['copy'] = array("kopieren","kopieren");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['delete'] = array("loeschen","loeschen");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['show'] = array("show","show");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['allgemein'] = array("Allgemein","Allgemein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['adresse'] = array("Adresse der Praxis","");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['anrede'] = array("Anrede","Geben Sie die Anrede ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['premium'] = array("Premiumeintrag","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['name'] = array("Name","Geben Sie den Namen ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['vorname'] = array("Vorname","Geben Sie den Vornamen ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['fachrichtung'] = array("Fachrichtung","Geben Sie die Fachrichtung ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['beruf'] = array("Beruf","W&auml;hlen Sie hier den entsprechenden Beruf aus");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['strasse'] = array("Stra&szlig;e","Geben Sie die Stra&szlig;e ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['plz'] = array("PLZ","Geben Sie die PLZ ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['ort'] = array("Ort","Geben Sie den Ort ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['land'] = array("Land","Geben Sie das Land ein");


$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['telefon'] = array("Telefon","Geben Sie die Telefonnummer ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['telefax'] = array("Telefax","Geben Sie die Telefaxnummer ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['email'] = array("E-Mail","Geben Sie die E-Mail-Adresse ein");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['homepage'] = array("Homepage","Geben Sie die Homepage an");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_montag_von'] = array("Montag (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_montag_bis'] = array("Montag (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_dienstag_von'] = array("Dienstag (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_dienstag_bis'] = array("Dienstag (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_mittwoch_von'] = array("Mittwoch (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_mittwoch_bis'] = array("Mittwoch (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_donnerstag_von'] = array("Donnerstag (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_donnerstag_bis'] = array("Donnerstag (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_freitag_von'] = array("Freitag (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_freitag_bis'] = array("Freitag (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_samstag_von'] = array("Samstag (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprechzeiten_samstag_bis'] = array("Samstag (2)","");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['sprachen'] = array("Sprachen","W&auml;hlen Sie hier die entsprechen Sprachen aus");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['schwerpunkte'] = array("Schwerpunkte","W&auml;hlen Sie hier die entsprechenden Schwerpunkte aus");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseiten'] = array("Unterseiten","");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite1_titel'] = array("Titel (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite1_inhalt'] = array("Inhalt (1)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite2_titel'] = array("Titel (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite2_inhalt'] = array("Inhalt (2)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite3_titel'] = array("Titel (3)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite3_inhalt'] = array("Inhalt (3)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite4_titel'] = array("Titel (4)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite4_inhalt'] = array("Inhalt (4)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite5_titel'] = array("Titel (5)","");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['unterseite5_inhalt'] = array("Inhalt (5)","");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['bild'] = array("Bild","Bild");

$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['override_title'] = array("Override Title","Override Title");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['override_description'] = array("Override Description","Override Description");
$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['override_keywords'] = array("Override Keywords","Override Keywords");

?>