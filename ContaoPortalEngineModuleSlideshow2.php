<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Aeron Glemann <http://www.electricprism.com/aeron/>
 * @package    Slideshow2 
 * @license    LGPL 
 * @filesource
 */


/**
 * Class ModuleSlideshow2
 *
 * Frontend module "slideshow2".
 * 
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de>
 * @package    Controller
 */
class ContaoPortalEngineModuleSlideshow2 extends Module
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_slideshow2';

	/**
	 * Thumbnail Sizes in Backend
	 * @var float
	 */
	protected $thumbWidth = 100;
	protected $thumbHeight = 50;	
	
	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
            
         
            
		// Create Preview when inserted as CE
		if (TL_MODE == 'BE')
		{
			$this->loadLanguageFile('tl_slideshow2');
			
			// Get Data
			$objBackend  = $this->Database->prepare("SELECT * FROM tl_slideshow2 WHERE id=?")
						   ->execute($this->slideshow2);

			$objElements = $this->Database->prepare("SELECT * FROM tl_slideshow2_elements WHERE pid=? ORDER by sorting ASC")
						   ->execute($objBackend->id);
					   
					   
			// Analyse input type (file or folder) and get image
			if ($objElements->numRows > 0)
			{
				while ($objElements->next())
				{
					if (strncmp($objElements->src, '.', 1) === 0)
					{
						continue;
					}
		
					// Directory
					if (is_dir(TL_ROOT . '/' . $objElements->src))
					{
						$subfiles = scan(TL_ROOT . '/' . $objElements->src);
						foreach ($subfiles as $subfile)
						{
							if (strncmp($subfile, '.', 1) === 0 || is_dir(TL_ROOT . '/' . $objElements->src . '/' . $subfile))
							{
								continue;
							}
			
							$objFile = new File($objElements->src . '/' . $subfile);
							if ($objFile->isGdImage)
							{
								$arrElements[$x]['src'] = $this->getImage($objElements->src . '/' . $subfile, $this->thumbWidth, $this->thumbHeight);
								$x++;
							}
						}
						continue;
					}
			
					// File
					if (is_file(TL_ROOT . '/' . $objElements->src))
					{
						$objFile = new File($objElements->src);
						if ($objFile->isGdImage)
						{
							$arrElements[$x]['src'] = $this->getImage($objElements->src, $this->thumbWidth, $this->thumbHeight);
							$x++;
						}
					}
		
				}			   
				
				// Create image array
				foreach ($arrElements as $element)
				{
					$OutputImages .= '<img src="' . $element['src'] . '" alt="' . $element['alt'] . '" style="padding-right:5px;padding-bottom:3px;" />';
				}
			}
			else
			{
				$OutputImages = $GLOBALS['TL_LANG']['tl_slideshow2']['misc_noimages'];
			}
						   
			// Output in Backend
			return '<div class="tl_gray" style="padding-bottom:16px;">### Slideshow 2 ###</div>
					<div class="tl_gray" style="padding-bottom:6px;"><a href="contao/main.php?do=slideshow2&amp;table=tl_slideshow2_elements&amp;id=1" title="Edit Slideshow">' . $objBackend->title . ' (' . count($arrElements) . ' ' . $GLOBALS['TL_LANG']['tl_slideshow2']['misc_images'] . ')</a></div>
				   ' . $OutputImages;
		}
	
                /*
		if (!$this->slideshow2)
		{
			return '';
		}
		*/
		return parent::generate();
	}
	
/*----- compile  -----------------------------------------------------------------*/

	/**
	 * Generate module
	 */
	protected function compile()
	{
           
            $this->slideshow2 = 1;
             
		
		//-- Create Data Arrays
		$objSettings 	= $this->Database->prepare("SELECT * FROM tl_slideshow2 WHERE id=?")
						  ->execute($this->slideshow2);
		
		$objElements 	= $this->Database->prepare("SELECT * FROM tl_slideshow2_elements WHERE pid=? ORDER by sorting ASC")
					   	  ->execute($this->slideshow2);
		
		//-- Prepare Data
		$arrSlideshow2Size 			= unserialize($objSettings->slideshow2_size);
		
		if($objSettings->thumbnails) 
		$arrSlideshow2ThumbSize     = unserialize($objSettings->thumbnails_size);
		
	 /** --------------------------------------------------------------------
	  * Generate HTML Image Array 
	  * -------------------------------------------------------------------- */
		
		//-- Reset Variables
		$x = 0;
		$url_link = false;
		$url_fullsize = false;
                
                
               // echo $GLOBALS['CPE-ID'];
                
                
                
                
                
                
                
              
                
                $fields = $this->Database->getFieldNames("tl_cpe_immobilien", $blnNoCache);
                
                $fields2 = array();
                foreach($fields as $field)
                {
                    if (substr($field,0,5) == "image")
                            $fields2[] = $field;
                    
                }
                
                
                
                $rs_images = $this->Database
                                    ->prepare("SELECT " .implode(",",$fields2). " from tl_cpe_immobilien where id =?")
                                    ->execute($GLOBALS['CPE-ID']);
                
                
                
                
                if (is_dir(TL_ROOT . '/' . $rs_images->$fields2[0]))
                {
                    $subfiles = scan(TL_ROOT . '/' . $rs_images->$fields2[0]);
                    
                    foreach ($subfiles as $subfile)
                    {
                            if (strncmp($subfile, '.', 1) === 0 || is_dir(TL_ROOT . '/' . $rs_images->$fields2[0] . '/' . $subfile))
                            {
                                    continue;
                            }
                            
                            $objFile = new File($rs_images->$fields2[0] . '/' . $subfile);

                                if ($objFile->isGdImage)
                                {
                                        //-- Imagenumber
                                        $arrElements[$x]['id'] = $x;

                                        //-- Thumbnails
                                        if ($objSettings->thumbnails)
                                        {
                                                $arrElements[$x]['thumb_src'] = $this->getImage($rs_images->$fields2[0] . '/' . $subfile, $arrSlideshow2ThumbSize[0], $arrSlideshow2ThumbSize[1]); 
                                                $arrElements[$x]['thumb_size'] = $arrSlideshow2ThumbSize;
                                        }
/*
                                        //-- Image resizing
                                        if ($objElements->img)
                                        {
                                                $arrElements[$x]['img_size'] = unserialize($objElements->img_size);
                                                $arrElements[$x]['src'] = $this->getImage($objElements->src . '/' . $subfile, $arrElements[$x]['img_size'][0], $arrElements[$x]['img_size'][1]);
                                        }
                                        else
                                        { */
                                                $arrElements[$x]['img_size'] = array($objFile->width,$objFile->height);
                                                $arrElements[$x]['src'] = $rs_images->$fields2[0] . '/' . $subfile;
                                       // }

                                        //-- Link
                                   /*     $arrElements[$x]['url'] 		 = $objElements->url;

                                        if ($objElements->url_link && $objElements->url)
                                        {
                                                $arrElements[$x]['url_link'] 		= ($objElements->url_link);
                                                $arrElements[$x]['url_title']		= $objElements->url_title;
                                                $arrElements[$x]['url_window']		= $objElements->url_window;

                                                $arrElements[$x]['url_fullsize'] 	= false;

                                                $url_link = true;
                                        }
                                        elseif ($objElements->url_fullsize && $objElements->url)
                                        {
                                                $arrElements[$x]['url_link'] 		= $objElements->src . '/' . $subfile;
                                                $arrElements[$x]['url_fullsize'] 	= true;

                                                $url_fullsize = true;
                                        }

                                        // Description and alt 
                                        if ($objSettings->captions_meta)
                                        {
                                                //-- Use Typolight Function to parse meta
                                                $this->parseMetaFile($objElements->src);
                                                $arrMeta = $this->arrMeta[$objFile->basename];

                                                // Take filename if there is no meta info for that file
                                                if ($arrMeta[2] == '')
                                                        $arrMeta[0] = str_replace('_', ' ', preg_replace('/^[0-9]+_/', '', $objFile->filename));

                                                $arrElements[$x]['description'] = $arrMeta[0];
                                                $arrElements[$x]['alt'] = $arrMeta[0];	
                                        }
                                        else
                                        {
                                                $arrElements[$x]['description'] = $objElements->description;
                                                $arrElements[$x]['alt'] = ($objElements->alt)? $objElements->alt : $objElements->description;
                                        }
*/
                                        //-- count image
                                        $x++;
                                }
                        }

                       
                            
                            
                            
                            
                            
                            
                            
                    }
                    
               
                else
                {
                

                    foreach ($fields2 as $field2)
                    {
                        if (strlen($field2) > 0)
                        {

                            if (is_file(TL_ROOT . '/' . $rs_images->$field2))
                            {
                                    $objFile = new File($rs_images->$field2);

                                    if ($objFile->isGdImage)
                                    {
                                            //-- Imagenumber
                                            $arrElements[$x]['id'] = $x;

                                            //-- Thumbnails
                                            if ($objSettings->thumbnails)
                                            {
                                                    $arrElements[$x]['thumb_src'] = $this->getImage($rs_images->$field2, $arrSlideshow2ThumbSize[0], $arrSlideshow2ThumbSize[1]); 
                                                    $arrElements[$x]['thumb_size'] = unserialize($objSettings->thumbnails_size);
                                            }


                                            /*

                                            //-- Image resizing
                                            if ($objElements->img)
                                            {
                                                    $arrElements[$x]['img_size'] = unserialize($objElements->img_size);
                                                    $arrElements[$x]['src'] = $this->getImage($rs_images->$field2, $arrElements[$x]['img_size'][0], $arrElements[$x]['img_size'][1]);
                                            }
                                            else
                                            { */
                                                    $arrElements[$x]['img_size'] = array($objFile->width,$objFile->height);
                                                    $arrElements[$x]['src'] = $rs_images->$field2;
                                            //}

                                                    /*
                                            //-- Link
                                            $arrElements[$x]['url'] = $objElements->url;

                                            if ($objElements->url_link && $objElements->url)
                                            {
                                                    $arrElements[$x]['url_link'] 		= ($objElements->url_link);
                                                    $arrElements[$x]['url_title']		= $objElements->url_title;
                                                    $arrElements[$x]['url_window']		= $objElements->url_window;

                                                    $arrElements[$x]['url_fullsize'] 	= false;

                                                    $url_link = true;
                                            }
                                            elseif ($objElements->url_fullsize && $objElements->url)
                                            {
                                                    $arrElements[$x]['url_link'] 		= $objElements->src;
                                                    $arrElements[$x]['url_fullsize'] 	= true;

                                                    $url_fullsize = true;
                                            }

                                            //echo dirname($objElements->src);

                                            // Description with or without meta file
                                            if ($objSettings->captions_meta)
                                            {
                                                    //-- Use Typolight Function to parse meta
                                                    $this->parseMetaFile(dirname($objElements->src));
                                                    $arrMeta = $this->arrMeta[$objFile->basename];

                                                    //print_r($arrMeta);

                                                    // Take filename if there is no meta info for that file
                                                    if ($arrMeta[2] == '')
                                                            $arrMeta[0] = str_replace('_', ' ', preg_replace('/^[0-9]+_/', '', $objFile->filename));

                                                    $arrElements[$x]['description'] = $arrMeta[0];
                                                    $arrElements[$x]['alt'] = $arrMeta[0];	
                                            }
                                            else
                                            {
                                                    $arrElements[$x]['description'] = $objElements->description;
                                                    $arrElements[$x]['alt'] = ($objElements->alt)? $objElements->alt : $objElements->description;
                                            }
                                            */
                                            //-- count image */
                                            $x++;
                                    }
                            }
                    }

                    }
                
                }
		
		//print_r($arrElements);
		
	 /** --------------------------------------------------------------------
	  * CSS Template
	  * --------------------------------------------------------------------*/ 
	
		/* Create CSS Template */
		$objTplCSS = new FrontendTemplate($objSettings->template_css);
	    
		$objTplCSS->id 						= $objSettings->id;
		$objTplCSS->arrSlideshow2Size 		= $arrSlideshow2Size;
		$objTplCSS->arrSlideshow2ThumbSize 	= $arrSlideshow2ThumbSize;
		$objTplCSS->totalElements			= count($arrElements)+1;
		
		if ($objSettings->controls) 		$objTplCSS->controls = true;
		if ($objSettings->captions) 		$objTplCSS->captions = true;
		
		if ($objSettings->thumbnails) 
		{
			$objTplCSS->thumbnails 						= true;
			$objTplCSS->arrSlideshow2ThumbPaddingWidth	= $objSettings->thumbnails_padding_width;
			$objTplCSS->arrSlideshow2ThumbPaddingHeight	= $objSettings->thumbnails_padding_height;			
		}
		
		if ($objSettings->play_image)
		{
				$play_number = ($objSettings->play_random)? mt_rand(0, count($arrElements)-1) : 0 ;
				$objTplCSS->play_image = $arrElements[$play_number]['src'];
		} 		
		
		//print_r($objTplCSS);
		
		/* Add CSS in HTML head */
		$GLOBALS['TL_HEAD'][] = $objTplCSS->parse();



	 /** --------------------------------------------------------------------
	  * JS Template
	  * --------------------------------------------------------------------*/

		//-- Create Image Array
		$elementsTotal = count($arrElements); 
		$x = 1;

		if (is_array($arrElements))
		{		
			foreach ($arrElements as $element)
			{
				$imgExtended = '';
				
				//-- Additional image data (Thumbnail and link)
				if ($objSettings->thumbnails) $imgExtended = ",thumbnail:'" . $element['thumb_src'] . "'";
				if ($element['url'] && $url_link)
				{
					$imgExtended .= ",href:'" . $element['url_link'] . "'";
				}
				else if($element['url'] && $url_fullsize)
				{ 
					$imgExtended .= ",href:'" . $element['src'] . "'";
				}
				
				$imgElements .= "'" .  $element['src']. "':{caption:'" . str_replace('/', '\/', $element['description']) . "'" . $imgExtended . "}";
				
				if ($x != $elementsTotal) $imgElements .= ","; 
				
				$x++;	
			}
		}

	  	//-- Effect type
		if ($objSettings->effect_type != 'Alpha')
		{
			$EffectType = '.' . $objSettings->effect_type;
			$this->Template->js_effect = 'plugins/slideshow2/js/slideshow.' . strtolower($objSettings->effect_type) . '.js';
		}	 
		
		//-- Extended
		if ($objSettings->effects_extended)
		{
			$EffectsExtended = 'transition: \'' . strtolower($objSettings->effect_transition) . ':' . $objSettings->effect_ease . '\',';
		}

		//-- Create JS Template 
		$objTplJS = new FrontendTemplate($objSettings->template_js);
	    
		$objTplJS->id 					= $objSettings->id;

		$objTplJS->imgElements 			= $imgElements;
		$objTplJS->arrSlideshow2Size 	= $arrSlideshow2Size;
		$objTplJS->EffectType 			= $EffectType;
		$objTplJS->effect_duration 		= $objSettings->effect_duration;
		$objTplJS->EffectsExtended 		= $EffectsExtended;
		$objTplJS->rotation_interval 	= $objSettings->rotation_interval;
		$objTplJS->controls_type		= $objSettings->controls_type;

	 	$objTplJS->play_loop	= ($objSettings->play_loop)		? 	'false' : 'true' ;
		$objTplJS->play_paused	= ($objSettings->play_paused)	? 	'true' : 'false' ;
		$objTplJS->play_random	= ($objSettings->play_random)	? 	'true' : 'false' ;
		$objTplJS->play_image	= ($objSettings->play_image)	?	'true' : 'false' ;
		$objTplJS->controls		= ($objSettings->controls)		? 	'true' : 'false' ;
		$objTplJS->thumbnails	= ($objSettings->thumbnails)	? 	'true' : 'false' ;
		$objTplJS->captions		= ($objSettings->captions)		? 	'true' : 'false' ;

		/* Add JS in HTML head 	*/	
		$GLOBALS['TL_HEAD'][] = $objTplJS->parse();					

	 /** --------------------------------------------------------------------
	  * mod_slideshow2 template
	  * -------------------------------------------------------------------- */
	  		
		$this->Template->fullsize = $url_fullsize;
		$this->Template->id = $objSettings->id;
		$this->Template->element = $arrElements[0];
		
	}
}