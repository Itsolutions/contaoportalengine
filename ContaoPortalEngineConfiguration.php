<?php



class ContalPortalEngineConfiguration extends Frontend
{

	private static $props = array();


	public function __construct(){

		parent::__construct();

		$configuration = $this->Database->query("SELECT * from tl_cpe_einstellungen ");

		while ($configuration->next())
		{
			$this->__set($configuration->name,$configuration->wert);
		}
	}


	public function __get($name){

		if(array_key_exists($name,self::$props)){
			return self::$props[$name];
		}
	}

	public function __set($name,$value)
	{
		self::$props[$name] = $value;
	}



}

?>