<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2013 Leo Feyer
 * 
 * @package ContaoCart
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'ContaoPortalEngine',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    
         // models
        'ContaoPortalEngine\CpeEinstellungenModel' => 'system/modules/ContaoPortalEngine/models/CpeEinstellungenModel.php',
    
	// Classes core
    
        'ContaoPortalEngine\ContaoPortalEngineRouter' => 'system/modules/ContaoPortalEngine/classes/ContaoPortalEngineRouter.php',
    
   //     'ContaoPortalEngine\CpeRouter' => 'system/modules/ContaoPortalEngine/models/CpeRouter.php',
        
    
//	'ContaoCart\ContaoCart' => 'system/modules/ContaoCart/classes/core/ContaoCart.php',
//        'ContaoCart\ContaoCartCart' => 'system/modules/ContaoCart/classes/core/ContaoCartCart.php',
//        'ContaoCart\ContaoCartCheckout' => 'system/modules/ContaoCart/classes/core/ContaoCartCheckout.php',
//        'ContaoCart\ContaoCartRouter' => 'system/modules/ContaoCart/classes/core/ContaoCartRouter.php',
//        'ContaoCart\ContaoCartConfiguration' => 'system/modules/ContaoCart/classes/core/ContaoCartConfiguration.php',
    
    
//    'ContaoCart\Payment' => 'system/modules/ContaoCart/classes/core/Payment.php',
    
        // Classes backend
//	'tl_cca_bestellungen'   => 'system/modules/ContaoCart/classes/backend/tl_cca_bestellungen.php',
//        'tl_cca_produkte'   => 'system/modules/ContaoCart/classes/backend/tl_cca_produkte.php',
      
        // Classes elements
        'ContaoPortalEngine\ImmobilienProKategorie' => "system/modules/ContaoPortalEngine/elements/ImmobilienProKategorie.php",
//        'ContaoCart\ContentProduct' => "system/modules/ContaoCart/elements/ContentProduct.php",
    
        // Classes widgets
//        'ContaoCart\MultiSelectWizard' => 'system/modules/ContaoCart/widgets/MultiSelectWizard.php',
    
    
      
        
        
    
));



/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
 //   'ccace_category'        => 'system/modules/ContaoCart/templates/',
 //   'cca_cart'        => 'system/modules/ContaoCart/templates/',
 //    'cca_checkout-1'        => 'system/modules/ContaoCart/templates/',
 //   'cca_checkout-2'        => 'system/modules/ContaoCart/templates/',
    'cpece_immobilienDetails' => 'system/modules/ContaoPortalEngine/templates/',
    'cpece_immobilienProKategorie' => 'system/modules/ContaoPortalEngine/templates/'
    
));



