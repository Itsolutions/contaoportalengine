<?php 


array_insert($GLOBALS['BE_MOD'], 0, array(
	'ContaoPortalEngine' => array(
	
		'Immobilien' => array(
                    'tables'		=>	array('tl_cpe_immobilien'),
		),
		
		'Kategorien' => array(
		    'tables'		=>	array('tl_cpe_kategorien'),
		),


				
		'Sprachen' => array(
                    'tables'		=>	array('tl_cpe_sprachen'),
		),
		
		'Einstellungen' => array(
		     'tables'		=>	array('tl_cpe_einstellungen'),
		),


		
)
));

/** 
 * Frontend modules
 */
$GLOBALS['FE_MOD']['miscellaneous']['ContaoPortalEngine'] = 'ContaoPortalEngine';
$GLOBALS['FE_MOD']['miscellaneous']['CPE-Slideshow2'] = 'ContaoPortalEngineModuleSlideshow2';
$GLOBALS['TL_CTE']['includes']['CPE-FlexibleList'] = 'ContaoPortalEngineFlexibleList';
$GLOBALS['TL_CTE']['includes']['CPE-ImmobilienProKategorie'] = 'ImmobilienProKategorie';


$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('ContaoPortalEngineRouter', 'replaceInsertTags');
$GLOBALS['TL_HOOKS']['getPageIdFromUrl'][] = array('ContaoPortalEngineRouter', 'myGetPageIdFromUrl');


?>