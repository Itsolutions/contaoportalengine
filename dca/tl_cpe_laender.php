<?php


$GLOBALS['TL_DCA']['tl_cpe_laender'] = array
(

	// Config
	'config' => array
	(
			'dataContainer'               => 'Table',
			'switchToEdit'                => true,

	),


	// Alle Einstellungen, die f�r die Auflistung bzw.
	// Manipulation von Datens�tzen ben�tigt werden.
	// Wir k�nnen hier die Sortierung, das Layout
	// (z. B. Filter, Suche, Einschr�nkungen)
	// Beschriftungen und Operationen bestimmen.

	'list' => array
	(
		// Sortierung
		'sorting' => array
		(
			'mode'                    => 1,
			// Sortierung nach name
			'fields'                  => array('land'),
			// Kategorisierung
			'flag'                    => 1,
			// Layout
			'panelLayout'             => 'sort,search,limit.'
		),

		// Beschriftung
		'label' => array
		(
 			'fields'                  => array('land'),
			'format'                  => '%s',

		),
	
	 	'global_operations' => array
		(
		
	 		'all' => array
			(
				 'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				 'href'                => 'act=select',
	 			 'class'               => 'header_edit_all',
	 			'attributes'          => 'onclick="Backend.getScrollOffset();"'
			)

		),

	 	'operations' => array
		(
		
		
		
	 		'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['edit'],
	 			'href'                => 'act=edit',
	 			'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['copy'],
	 			'href'                => 'act=copy',
	 			'icon'                => 'copy.gif',
			),
	 		'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['delete'],
	 			'href'                => 'act=delete',
	 			'icon'                => 'delete.gif',
	 			'attributes'          => 'onclick="if (!confirm(\'' .
					$GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))
	   			 return false; Backend.getScrollOffset();"',
			),
	 		'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['show'],
	 			'href'                => 'act=show',
	 			'icon'                => 'show.gif'
			)
		),
	), // list end

	
 	'palettes' => array
	(
		'default'                     => '{Allgemein},land;'
										 
	),

 	'fields' => array
	(
		// Felder, die im Backend angezeigt werden sollen.
		'land' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_laender']['land'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>true, 'maxlength'=>50)
		),


		

	)
);
