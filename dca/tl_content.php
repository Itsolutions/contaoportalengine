<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');



$GLOBALS['TL_DCA']['tl_content']['palettes']['CPE-ImmobilienProKategorie'] = '{title_legend},type;{Feldzuordnungen},cpe_kategorie;cpe_kategorie2;cpe_kategorie3;cpe_kategorie4,cpe_title';
$GLOBALS['TL_DCA']['tl_content']['fields']['cpe_kategorie'] = array(
	
	'label'                    => array("Kategorie 1","Wählen Sie die entsprechende Kategorie aus"),
	'inputType'                => 'select',
        'foreignKey'               => "tl_cpe_kategorien.name_de",
        'sql' => "int(10) unsigned NOT NULL default '0'"
	
);

$GLOBALS['TL_DCA']['tl_content']['fields']['cpe_kategorie2'] = array(
	
	'label'                    => array("Kategorie 2","Wählen Sie die entsprechende Kategorie aus"),
	'inputType'                => 'select',
        'foreignKey'               => "tl_cpe_kategorien.name_de",
        'sql' => "int(10) unsigned NOT NULL default '0'"
	
);

$GLOBALS['TL_DCA']['tl_content']['fields']['cpe_kategorie3'] = array(
	
	'label'                    => array("Kategorie 3","Wählen Sie die entsprechende Kategorie aus"),
	'inputType'                => 'select',
        'foreignKey'               => "tl_cpe_kategorien.name_de",
        'sql' => "int(10) unsigned NOT NULL default '0'"
        
	
);

$GLOBALS['TL_DCA']['tl_content']['fields']['cpe_kategorie4'] = array(
	
	'label'                    => array("Kategorie 4","Wählen Sie die entsprechende Kategorie aus"),
	'inputType'                => 'select',
        'foreignKey'               => "tl_cpe_kategorien.name_de",
        'sql' => "int(10) unsigned NOT NULL default '0'"
      
        
	
);

$GLOBALS['TL_DCA']['tl_content']['fields']['cpe_title'] = array(
		
			 'label'                   => "alternative Überschrift",
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>true, 'maxlength'=>255),
			   'sql'                     => "varchar(255) NOT NULL default ''"
			 
);

?>