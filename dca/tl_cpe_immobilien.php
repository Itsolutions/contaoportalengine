<?php


$GLOBALS['TL_DCA']['tl_cpe_immobilien'] = array
(

	// Config
	'config' => array
	(
			'dataContainer'               => 'Table',
			'switchToEdit'                => true,
            'onsubmit_callback' => array
                (
                        array('tl_cpe_immobilien', 'buildSeoUrls')
                ) ,
         'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
        
            )
        )

	),


	// Alle Einstellungen, die f�r die Auflistung bzw.
	// Manipulation von Datens�tzen ben�tigt werden.
	// Wir k�nnen hier die Sortierung, das Layout
	// (z. B. Filter, Suche, Einschr�nkungen)
	// Beschriftungen und Operationen bestimmen.

	'list' => array
	(
		// Sortierung
		'sorting' => array
		(
			'mode'                    => 1,
			// Sortierung nach name
			'fields'                  => array('region_de'),
			// Kategorisierung
			'flag'                    => 1,
			// Layout
			'panelLayout'             => 'sort,search,limit.'                                                                             
		),

		// Beschriftung
		'label' => array
		(
 			'fields'                  => array('bezeichnung',),
                        'label_callback'          => array('tl_cpe_immobilien', 'generateLabel')

		),
	
	 	'global_operations' => array
		(
		
	 		'all' => array
			(
				 'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				 'href'                => 'act=select',
	 			 'class'               => 'header_edit_all',
	 			'attributes'          => 'onclick="Backend.getScrollOffset();"'
			)

		),

	 	'operations' => array
		(
		
		
		
	 		'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['edit'],
	 			'href'                => 'act=edit',
	 			'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['copy'],
	 			'href'                => 'act=copy',
	 			'icon'                => 'copy.gif',
			),
	 		'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['delete'],
	 			'href'                => 'act=delete',
	 			'icon'                => 'delete.gif',
	 			'attributes'          => 'onclick="if (!confirm(\'' .
					$GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))
	   			 return false; Backend.getScrollOffset();"',
			),
	 		'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['show'],
	 			'href'                => 'act=show',
	 			'icon'                => 'show.gif'
			)
		),
	), // list end

	
 	'palettes' => array
	(
            
            'default' => '{Basisdaten},objektnummer,name,kategorie,ort;'.
                         '{Bezeichung},bezeichnung_de,bezeichnung_en,bezeichnung_fr,bezeichnung_es,bezeichnung_ru;'.
                         '{Bezeichung2},bezeichnung2_de,bezeichnung2_en,bezeichnung2_fr,bezeichnung2_es,bezeichnung2_ru;'.
                         '{Bereich},bereich_de,bereich_en,bereich_fr,bereich_es,bereich_ru;'.
                         '{Region},region_de,region_en,region_fr,region_es,region_ru;'.
                         '{Details},flaeche,grundstueckflaeche,preis,preisgebot,mieteinnahmen;'.
                         '{Bilder},thumbnail,image1,image2,image3,image4,image5,image6;'.
                         '{Texte},freitext_de,freitext_en,freitext_es;'
            

	),

 	'fields' => array
	(
		// Felder, die im Backend angezeigt werden sollen.


        'id' => array(
            'sql'   =>   "int(10) unsigned NOT NULL auto_increment"
        ),

                'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),


        'objektnummer' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['objektnummer'] ,
                        'inputType'               => 'text',
                        'eval'                    => array('mandatory'=>true),
                        'sql'                     => "varchar(32) NOT NULL default ''"
		),  

        'name' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['name'] ,
                        'inputType'               => 'text',
                        'search'                  => true,
                        'sql'                     => "varchar(255) NOT NULL default ''"
        ),  

        'bezeichnung_de' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_de'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),  
            
        'bezeichnung_en' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_en'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),             
            
        'bezeichnung_es' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_es'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ), 


        'bezeichnung_fr' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_fr'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ), 

        'bezeichnung_ru' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung_ru'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ), 
    
        'kategorie' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['kategorie'] ,
                        'inputType'               => 'select',
                        'foreignKey'              => "tl_cpe_kategorien.name_de",
                        //'relation'                => array('type'=>'hasOne', 'load'=>'eager'),
   
                        'eval'                    => array('mandatory'=>true),
                         'sql' => "int(10) unsigned NOT NULL default '0'"
        ), 
            
        
        'bezeichnung2_de' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_de'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                      
        ),  
            
        
        'bezeichnung2_en' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_en'] ,
                        'inputType'               => 'text',                           
                        'sql'                     => "varchar(255) NOT NULL default ''"
        ),             
            
        
        'bezeichnung2_es' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_es'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),

        'bezeichnung2_fr' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_fr'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                                  
        ), 

        'bezeichnung2_ru' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bezeichnung2_ru'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),



        'bereich_de' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bereich_de'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                      
        ),  
            
        
        'bereich_en' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bereich_en'] ,
                        'inputType'               => 'text',                           
                        'sql'                     => "varchar(255) NOT NULL default ''"
        ),             
            
        
        'bereich_es' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bereich_es'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),

        'bereich_fr' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bereich_fr'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                                  
        ), 

        'bereich_ru' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['bereich_ru'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),



        'region_de' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['region_de'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                      
        ),  
            
        
        'region_en' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['region_en'] ,
                        'inputType'               => 'text',                           
                        'sql'                     => "varchar(255) NOT NULL default ''"
        ),             
            
        
        'region_es' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['region_es'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),

        'region_fr' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['region_fr'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                                  
        ), 

        'region_ru' => array
        (
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['region_ru'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''"                       
        ),



/*
            
        'ort' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['ort'] ,
                        'inputType'               => 'text',
                        'sql'                     => "varchar(255) NOT NULL default ''" ,                       
                        'eval'                    => array('mandatory'=>true)
		),  
            
            
        'flaeche' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['flaeche'] ,
                        'inputType'               => 'text',
                        'eval'                    => array('mandatory'=>true)
		), 
        
        'grundstueckflaeche' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['grundstueckflaeche'] ,
                        'inputType'               => 'text',
     
		),            
  */              
        
        'preis' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['preis'] ,
                        'inputType'               => 'text',
                        'eval'                    => array('mandatory'=>true),
                        'sql'                     => "double unsigned NOT NULL default '0'"        
		),
            
    /*    
        'preisgebot' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['preisgebot'] ,
                        'inputType'               => 'checkbox'
             
		), 

        
        'mieteinnahmen' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['mieteinnahmen'] ,
                        'inputType'               => 'text',       
		),
            
        
        'asset' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['asset'] ,
                        'inputType'               => 'text',
        ),
            
            
        'thumbnail' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['thumbnail'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'filesOnly'=>true)
        ),
    
        'image1' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image1'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'folders'=>true)
        ),
        'image2' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image2'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'filesOnly'=>true)
        ),
        'image3' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image3'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'filesOnly'=>true)
        ),
        'image4' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image4'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'filesOnly'=>true)
        ),  
        'image5' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image5'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'filesOnly'=>true)
        ),
        'image6' => array
        (
                'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['image6'],
                'exclude'                 => true,
                'inputType'               => 'fileTree',
                'eval'                    => array('fieldType'=>'radio', 'files'=>true, 'filesOnly'=>true)
        ),            
            
      */    
        'freitext_de' => array
		(
            'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_de'] ,
            'inputType'               => 'textarea',
			'search'                  => true,
			'eval'                    => array('rte'=>'tinyMCE'),
            'sql'                     => "varchar(255) NOT NULL default ''"     
		),
        'freitext_en' => array
		(
            'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_en'] ,
            'inputType'               => 'textarea',
			'search'                  => true,
			'eval'                    => array('rte'=>'tinyMCE'),
            'sql'                     => "varchar(255) NOT NULL default ''"     
		),
        
        'freitext_es' => array
		(
            'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_es'] ,
            'inputType'               => 'textarea',
			'search'                  => true,
			'eval'                    => array('rte'=>'tinyMCE'),
            'sql'                     => "varchar(255) NOT NULL default ''"     
		), 

        'freitext_fr' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_fr'] ,
            'inputType'               => 'textarea',
            'search'                  => true,
            'eval'                    => array('rte'=>'tinyMCE'),
            'sql'                     => "varchar(255) NOT NULL default ''"     
        ), 

        'freitext_ru' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['freitext_ru'] ,
            'inputType'               => 'textarea',
            'search'                  => true,
            'eval'                    => array('rte'=>'tinyMCE'),
            'sql'                     => "varchar(255) NOT NULL default ''"     
        ), 
            

           /* 
            'hash' => array
		(
                        'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['hash'] ,
                         'inputType'               => 'text',
			 'search'                  => true,

		),  
            
            'updatelock' => array 
             (
                      'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_immobilien']['updatelock'] ,
                        'inputType'               => 'checkbox'
             ),
            
*/

		) 
);

class tl_cpe_immobilien extends Backend
{
    
    public function getKategoriename($kategorie_id)
    {
        return $this->Database->prepare("SELECT name_de from tl_cpe_kategorien where id =?")->execute($kategorie_id)->name_de;
    }
    
    
    
    public function generateLabel($dc)
    {
        $rsImmobilien = $this->Database->prepare("SELECT * from tl_cpe_immobilien where id = ?")->execute($dc['id']);
        
        $str = $rsImmobilien->region_de . " - ".$rsImmobilien->bereich_de. " - ".$rsImmobilien->name;
        
        
        
        
 
        
        
        return $str;
    }
    
    public function buildSeoUrls(DataContainer $dc)
    {
       // $this->Database->prepare("truncate table tl_cpe_seourls_immobilien")->execute();

		
        $einstellung= $this->Database->prepare("SELECT wert from tl_cpe_einstellungen where name = 'URL_Detailseite' ")->execute()->wert;

        
        //echo $einstellung;exit();

        preg_match_all("/%(.*)%/Ui", $einstellung, $efelder);


        $s = implode(",",$efelder[1]);

       


      //  $rs = $this->Database->prepare("select ".$s."  from tl_cpe_immobilien")->execute();
                
             
		
       // while ($rs->next())
       // {

                $surl = $einstellung;	


//print_r($dc->activeRecord);exit();


                foreach($efelder[1] as $efeld)
                {
                  
                    $surl = str_replace("%".$efeld."%", $dc->activeRecord->$efeld, $surl);
                    
                }
                    

                $surl = str_replace(".","",$surl);
                $surl = strtolower($surl);
                $surl = urlencode($surl);





               $rs =  $this->Database->prepare("insert ignore into tl_cpe_seourls_immobilien values (?,?,?,?,?)")->execute("0",$dc->activeRecord->id, $surl,'','');


                echo $rs->query;


                    exit();

       // } 
    }
}