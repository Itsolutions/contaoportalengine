<?php


$GLOBALS['TL_DCA']['tl_cpe_einstellungen'] = array
(

	// Config
	'config' => array
	(
			'dataContainer'               => 'Table',
			'switchToEdit'                => true,

			  'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
        
            )
        )


	),


	// Alle Einstellungen, die f�r die Auflistung bzw.
	// Manipulation von Datens�tzen ben�tigt werden.
	// Wir k�nnen hier die Sortierung, das Layout
	// (z. B. Filter, Suche, Einschr�nkungen)
	// Beschriftungen und Operationen bestimmen.

	'list' => array
	(
		// Sortierung
		'sorting' => array
		(
			'mode'                    => 1,
			// Sortierung nach name
			'fields'                  => array('name'),
			// Kategorisierung
			'flag'                    => 1,
			// Layout
			'panelLayout'             => 'sort,search,limit.'
		),

		// Beschriftung
		'label' => array
		(
 			'fields'                  => array('name'),
			'format'                  => '%s',

		),
	
	 	'global_operations' => array
		(
		
	 		'all' => array
			(
				 'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				 'href'                => 'act=select',
	 			 'class'               => 'header_edit_all',
	 			'attributes'          => 'onclick="Backend.getScrollOffset();"'
			)

		),

	 	'operations' => array
		(
		
		
		
	 		'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_schwerpunkte']['edit'],
	 			'href'                => 'act=edit',
	 			'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['copy'],
	 			'href'                => 'act=copy',
	 			'icon'                => 'copy.gif',
			),
	 		'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['delete'],
	 			'href'                => 'act=delete',
	 			'icon'                => 'delete.gif',
	 			'attributes'          => 'onclick="if (!confirm(\'' .
					$GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))
	   			 return false; Backend.getScrollOffset();"',
			),
	 		'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_zahnaerzte']['show'],
	 			'href'                => 'act=show',
	 			'icon'                => 'show.gif'
			)
		),
	), // list end

	
 	'palettes' => array
	(
		'default'                     => '{Allgemein},name,wert;'
										 
	),

 	'fields' => array
	(

		        'id' => array(
            'sql'   =>   "int(10) unsigned NOT NULL auto_increment"
        ),

        'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),




		// Felder, die im Backend angezeigt werden sollen.
		'name' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_einstellungen']['name'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>true, 'maxlength'=>255),
			  'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'wert' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_einstellungen']['wert'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>true, 'maxlength'=>255),
			  'sql'                     => "varchar(255) NOT NULL default ''"
		),

		

	)
);
