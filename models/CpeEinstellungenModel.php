<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package Core
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ContaoPortalEngine;


/**
 * Reads and writes users
 *
 * @package   Models
 * @author    Leo Feyer <https://github.com/leofeyer>
 * @copyright Leo Feyer 2005-2013
 */


if (!class_exists("CpeEinstellungenModel"))
{

    class CpeEinstellungenModel extends \Model
    {

            /**
             * Table name
             * @var string
             */
            protected static $strTable = 'tl_cpe_einstellungen';
            
            
       
            
            
            public static function findBy($strColumn, $varValue, array $arrOptions = array()) {
                
                if($varValue == "multilanguage_used")
                {
                    
                   $session = \Session::getInstance();
                   
                   if (!$session->get("multilanguage_used"))
                   {
                       
                       $rs = \Database::getInstance()
                                 ->prepare("SELECT distinct language from tl_page where published= 1 and language <> ''")->execute();
                     
                       if($rs->count() > 1)
                           $multilanguage_used = true;
                       else
                           
                           $multilanguage_used = false;
                          
                        $session->set("multilanguage_used",$multilanguage_used);
                           
                   } else {
                       
                        $multilanguage_used = $session->get("multilanguage_used");
                       
                   }   
                    
                    
                   $retval = new \stdClass();
                   $retval->wert = $multilanguage_used;
                   
            
                    
                    return $retval;
                }    
                
                else
                
                    return parent::findBy($strColumn, $varValue, $arrOptions);
            }
            
            
            
    }


}