<?php

class ContaoPortalEngineDCAHelper extends Backend {

    public function __construct() {
        parent::__construct();
    }

    public function loadLanguages(DataContainer $dc) {

        $boxes = trimsplit(';', $GLOBALS['TL_DCA'][$dc->table]['palettes']['default']);
        $legends = array();
        
        if (!empty($boxes)) {
            foreach ($boxes as $k => $v) {
                
                $eCount = 1;
                $boxes[$k] = trimsplit(',', $v);
/*
                foreach ($boxes[$k] as $kk => $vv) {
                    if (preg_match('/^\[.*\]$/i', $vv)) {
                        ++$eCount;
                        continue;
                    }

                    if (preg_match('/^\{.*\}$/i', $vv)) {
                        $legends[$k] = substr($vv, 1, -1);
                        unset($boxes[$k][$kk]);
                    } elseif ($GLOBALS['TL_DCA'][$dc->table]['fields'][$vv]['exclude'] || !is_array($GLOBALS['TL_DCA'][$dc->table]['fields'][$vv])) {
                        unset($boxes[$k][$kk]);
                    }
                }

                // Unset a box if it does not contain any fields
                if (count($boxes[$k]) < $eCount) {
                    unset($boxes[$k]);
                } */
            }
        }
        
        
        
        // Sprachen ermitteln
        $rsSprachen = $this->Database->prepare("SELECT * from tl_cpe_sprachen")->execute();
        


        
        
        foreach ($GLOBALS['TL_DCA'][$dc->table]['fields'] as $key => $field) {
            
           
            
            
            
            if ($field['eval']['multilang'] == 'true')
            {

                    $c = 0;
                    while ($rsSprachen->next())
                    {
                        
                        $sprachen[$c]['kuerzel'] = $rsSprachen->kuerzel;
                        $sprachen[$c]['name'] = $rsSprachen->name; 
                        $GLOBALS['TL_DCA'][$dc->table]['fields'][$key."_".$sprachen[$c]['kuerzel']] = 
                             $GLOBALS['TL_DCA'][$dc->table]['fields'][$key];
                        
                        
                        $GLOBALS['TL_LANG']['MOD'][$dc->table][$key."_".$sprachen[$c]['kuerzel']] = 
                                
                                    array("Name","Geben Sie hier den Namen der Kategorien ein");
                        
                        $c++;
                    }
                    
                     $rsSprachen->first();
                     unset($GLOBALS['TL_DCA'][$dc->table]['fields'][$key."_".$sprachen[$c]['kuerzel']]);
            }
            
            
            
           
        }
        
        
           
        

      //  var_dump($GLOBALS['TL_LANG']['MOD'][$dc->table]);

      //exit();
    }

}

?>
