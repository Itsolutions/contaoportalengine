<?php

namespace ContaoPortalEngine;

require_once TL_ROOT ."/system/modules/ContaoPortalEngine/languages/".$GLOBALS['TL_LANGUAGE']. "/ContaoPortalEngine.php";


class ContaoPortalEngineRouter extends \Frontend {

    protected function replaceInsertTags($strBuffer, $blnCache=false) {

	$GLOBALS['CURRENT_CPE_TEMPLATE'];
	$arr = explode("::",$strBuffer);
		
        switch ($arr[0]) {
            
                case "cpe-file" : 
                    ob_start();
                    include('templates/' . $arr[1]);
                    $arrCache[$arr[1]] = ob_get_contents();
                    ob_end_clean();break;
        }
        
        return $arrCache[$arr[1]];
    } 
    
    public function myGetPageIdFromUrl($arrFragments) {

  

        

        $rs = $this->Database
                   ->prepare(    
                           "SELECT seo.*, immo.*, p.alias as alias from tl_cpe_seourls_immobilien seo  
                            INNER JOIN tl_cpe_immobilien immo on seo.idImmobilien = immo.id   
                            INNER JOIN tl_content c on immo.kategorie = c.cpe_kategorie
                            INNER JOIN tl_article a on a.id = c.pid 
                            INNER JOIN tl_page p on p.id = a.pid
                            where seourl = ?
                            ")
                   ->execute($arrFragments[0]);
     //   echo $rs->query;
        
        
      //  exit();
       
           
        if ($rs->idImmobilien) {
               
            $GLOBALS['CPE-TARGET'] = 'cpe-detail';
            $GLOBALS['CPE-ID'] = $rs->idImmobilien;
            $this->getFrontendModule(6);
            
            
       //     echo $rs->alias;
            return array($rs->alias);
        }
        else
            return $arrFragments;
    }


} 


?>