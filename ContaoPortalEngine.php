<?php

if (!defined('TL_ROOT')) die('You cannot access this file directly!');


class ContaoPortalEngine extends Module
{
	/**
	* Template
	* @var string
	*/
	

	
	protected $strTemplate = 'ContaoPortalEngine';
        
        
      
                
        
        
        
        public function generateGallery2(&$rs)
        {
            $images = array();
			
			$gallery = "";
         
            if ($rs->image1) $images[] = $rs->image1;
            if ($rs->image2) $images[] = $rs->image2;
            if ($rs->image3) $images[] = $rs->image3;
            if ($rs->image4) $images[] = $rs->image4;
            if ($rs->image5) $images[] = $rs->image5;
            if ($rs->image6) $images[] = $rs->image6; 
            
            $c = 0;
            foreach($images as $image)
            {
                $gallery .=  "<a onclick=\"Mediabox.open([";
            
                $b = 1;
                
                for ($i=$c;$i<count($images);$i++)
                {
                    if ($b > 1)
                        $gallery .=",";
                    $gallery.= "['".$images[$i]."','Bild". ($i+1) . "','']";

                    $b++;
                }
				
				for ($i=$c;$i>1;$i--)
				{
					if ($b > 1)
                        $gallery .=",";
                    $gallery.= "['".$images[$i]."','Bild". ($i+1) . "','']";

                    $b++;
				}
				
                $gallery.= "],0);\" title='' rel='' class='hyperlink_txt' href='javascript:void(null)'><img width='250px' src='".$images[$c]."'/></a> ";

                
                
                $c++;
            }
            return $gallery;
            
        }
        
        
        public function generateGallery(&$rs)
        {
            $images = array();
         
            if ($rs->image1) $images[] = $rs->image1;
            if ($rs->image2) $images[] = $rs->image2;
            if ($rs->image3) $images[] = $rs->image3;
            if ($rs->image4) $images[] = $rs->image4;
            if ($rs->image5) $images[] = $rs->image5;
            if ($rs->image6) $images[] = $rs->image6;
            
            
            
        
            
            $gallery =  "<a onclick=\"Mediabox.open([";
            
            $b = 1;
            foreach($images as $image)
            {
                if ($b > 1)
                    $gallery .=",";
                $gallery.= "['".$image."','Bild". $b. "','']";
                
                $b++;
            }    
            
            $gallery.= "],0);\" title='' rel='' class='hyperlink_txt' href='javascript:void(null)'><img width='250px' src='".$images[0]."'/></a> ";
            
            
            return $gallery;
            
        }
	
	public function generate()
	{
            
            
        
            
           
            
     //       print_r($this->getPageDetails(8));

         
        
            require_once dirname(__FILE__)."/languages/".$GLOBALS['TL_LANGUAGE']. "/ContaoPortalEngine.php";
            
             // print_r($_SESSION);exit();
		
		if (TL_MODE == 'BE')
		{
			return;
		}
		
	//	$_SESSION['REQUEST_TOKEN']['FE'][] = $_POST['REQUEST_TOKEN'];
		
                
     
      
		
		// Hauptsteuerung
		switch ($GLOBALS['CPE-TARGET'])
		{
			case "cpe-index":
				$this->strTemplate = 'cpe-index';
				break;
			case "cpe-list":
				$this->strTemplate = 'cpe-list';
				break;
			case "cpe-detail":
				$this->strTemplate = 'cpe-detail';
				break;
                        
		
		}		

		
		return parent::generate();

	}
	
	/**
	* Generate module
	*/
	protected function compile()
	{
            
       
                
		switch ($GLOBALS['CPE-TARGET'])
		{
			case "cpe-index":
				$this->generateCPEIndex($this->Template);
                                $this->generateCPEList($this->Template);
                            
				break;
			case "cpe-list":
				$this->generateCPEList($this->Template);
				break;
			case "cpe-detail":
				$this->generateCPEDetail($this->Template);
				break;
		
		}
		
		
		$this->Template->GoogleMapsKey = $GLOBALS['CPE-Conf']->GoogleMapsKey;
						
		$GLOBALS['CURRENT_CPE_TEMPLATE'] = $this->Template;
		
		

		
	}
	
	private function generateCPEDetail(&$Template)
	{
		global $objPage;

              
                // Hauptabfrage
                $sqla = 
			"select  immo.*, kat.*,
                                
                                 kat.name_{$GLOBALS['TL_LANGUAGE']} as kategoriename,
                               
                                 immo.freitext_{$GLOBALS['TL_LANGUAGE']} as freitext

                                    from tl_cpe_immobilien as immo
                            inner join tl_cpe_kategorien as kat on kat.id = immo.kategorie
                       
                                 where immo.id = '".$GLOBALS['CPE-ID']."'";
 
                                 
                
                $rs = $this->Database->prepare($sqla)->execute($GLOBALS['CPE-ID']);
                
                $this->Template->fBezeichnung = "bezeichnung_".$GLOBALS['TL_LANGUAGE'];
                $this->Template->fBezeichnung2 = "bezeichnung2_".$GLOBALS['TL_LANGUAGE'];
                
				
		
		$this->Template->rs = $rs;
	}	
	
	
	private function generateCPEList(&$Template)
	{
            $whereT = array();
            
            
            if ($_POST['selHersteller'] > 0)
                $whereT['fz.hersteller'] = $_POST['selHersteller'];
            
            if ($_POST['selKategorie'] > 0)
                $whereT['fz.kategorie'] = $_POST['selKategorie'];
             
            if ($_POST['selFahrzeugart'] > 0)
                $whereT['fz.fahrzeugart'] = $_POST['selFahrzeugart'];
            
            if ($_POST['selAnzahlAchsen'] > 0)
                $whereT['fz.anzahl_achsen'] = $_POST['selAnzahlAchsen'];
             
                
            
            
            // Hauptabfrage
            $sqla = 
			"select  fz.*, 
                                 her.name as herstellername,
                                 fz.id as fahrzeugid


                                    from tl_cpe_fahrzeuge as fz
                            inner join tl_cpe_kategorien as kat on kat.id = fz.kategorie
                            inner join tl_cpe_hersteller as her on her.id = fz.hersteller
                            inner join tl_cpe_fahrzeugarten as fza on fza.id = fz.fahrzeugart";
            
            
            $c = 0;
            
            foreach ($whereT as $k=>$v)
            {
                if ($c == 0)
                    $sqla .= " WHERE ";
                else
                    $sqla .= " AND ";
                
                $sqla .= $k . " = '".$v."' ";
                
                $c++;
                
            }
            
            if ($_POST['selBaujahr'] > 0 )
            {
                $von = mktime(0,0,0,1,1,$_POST['selBaujahr']);
                $bis = mktime(0,0,0,12,31,$_POST['selBaujahr']);
                
               $sqla.=" AND erstzulassung BETWEEN {$von} AND {$bis} ";
            }
            
       
          
		
            
            /*
            
		if ($this->Input->post("selLand"))
			$selLand = $_SESSION['selLand'] = $this->Input->post("selLand");
		else if ($_SESSION['selLand'])
			$selLand = $_SESSION['selLand'];
			
		if ($this->Input->post("selSprache"))
			$selSprache = $_SESSION['selSprache'] =  $this->Input->post("selSprache");
		else if ($_SESSION['selSprache'])
			$selSprache = $_SESSION['selSprache'];
		
		if ($this->Input->post("selSchwerpunkt"))
			$selSchwerpunkt = $_SESSION['selSchwerpunkt'] =  $this->Input->post("selSchwerpunkt");
		else if ($_SESSION['selSchwerpunkt'])
			$selSchwerpunkt = $_SESSION['selSchwerpunkt'];
		
		if ($this->Input->post("umkreis"))
			$umkreis = $_SESSION['umkreis'] = $this->Input->post("umkreis");
		else if ($_SESSION['umkreis'])
			$umkreis = $_SESSION['umkreis'];
		
		if ($this->Input->post("selUmkreis"))
			$selUmkreis = $_SESSION['selUmkreis'] = $this->Input->post("selUmkreis");
		else if ($_SESSION['selUmkreis'])
			$selUmkreis = $_SESSION['selUmkreis'];		
		
		
		*/



		
		/* Hauptabfrage ohne Umkreissuche zusammenstellen
		
		
			where zasp.idSprachen = {$selSprache}
			and zaschw.idSchwerpunkte = {$selSchwerpunkt}
			and land = {$selLand} 
			order by za.premium desc";
		*/	
			
		
		
		$rs = $this->Database->prepare($sqla)->execute();
		

	    
		
		
		// Pagination
		
		
		
		if ($GLOBALS['CPE-Conf']->Anzahl_Datensaetze_pro_Seite > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			$limit = $GLOBALS['CPE-Conf']->Anzahl_Datensaetze_pro_Seite;
			$offset = ($page - 1) * $GLOBALS['CPE-Conf']->Anzahl_Datensaetze_pro_Seite;
 
			
		
			// Initialize the pagination menu
			$objPagination = new Pagination($rs->numRows, $GLOBALS['CPE-Conf']->Anzahl_Datensaetze_pro_Seite);
			$this->Template->pagination = $objPagination->generate("\n  ");
			
			$rs2 = $this->Database->prepare($sqla)-> limit($GLOBALS['CPE-Conf']->Anzahl_Datensaetze_pro_Seite,($page-1)*$GLOBALS['CPE-Conf']->Anzahl_Datensaetze_pro_Seite) ->execute();
			$this->Template->rs = $rs2;
		}
		else
			$this->Template->rs = $rs;
	
		

		
		$this->Template->anzahl = $rs->numRows;
		$this->Template->modulordner = $GLOBALS['CPE-Conf']->URL_Modulordner."/";
		
	  	
		
	}
	private function generateCPEIndex(&$Template)
	{

            
            
		// fsmKategorie
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selKategorie";
		$form1->id = "selKategorie";
                $form1->value = $_POST['selKategorie'];
		
		$rs = $this->Database->query("
					SELECT distinct tl_cpe_fahrzeuge.kategorie as id ,tl_cpe_kategorien.name_{$GLOBALS['TL_LANGUAGE']} as name
				    FROM tl_cpe_fahrzeuge 
				    INNER JOIN tl_cpe_kategorien on tl_cpe_fahrzeuge.kategorie =  tl_cpe_kategorien.id
				");
                
               
		$tmpArray = array();
                $tmpArray[] = array ("label" => $GLOBALS['TL_LANG']['MOD']['ContaoPortalEngine']['alle'], "value" => 0);
                
		while ($rs->next())
		{
			$tmpArray[] = array ("label" => $rs->name, "value" => $rs->id);
		}
		$form1->options = serialize($tmpArray);
		$Template->fsmKategorien = $form1->generate();
		
                
  		// fsmHersteller
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selHersteller";
		$form1->id = "selHersteller";
                $form1->value = $_POST['selHersteller'];
		
		$rs = $this->Database->query("
					SELECT distinct tl_cpe_fahrzeuge.hersteller as id ,tl_cpe_hersteller.name as name
				    FROM tl_cpe_fahrzeuge 
				    INNER JOIN tl_cpe_hersteller on tl_cpe_fahrzeuge.hersteller =  tl_cpe_hersteller.id
				");
                
               
		$tmpArray = array();
                $tmpArray[] = array ("label" => $GLOBALS['TL_LANG']['MOD']['ContaoPortalEngine']['alle'], "value" => 0);
		while ($rs->next())
		{
			$tmpArray[] = array ("label" => $rs->name, "value" => $rs->id);
		}
		$form1->options = serialize($tmpArray);
		$Template->fsmHersteller = $form1->generate();        
                
                

                // fsmFahrzeugart
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selFahrzeugart";
                $form1->id = "selFahrzeugart";
                 $form1->value = $_POST['selFahrzeugart'];
		
		$rs = $this->Database->query("
					SELECT distinct tl_cpe_fahrzeuge.fahrzeugart as id ,tl_cpe_fahrzeugarten.name_{$GLOBALS['TL_LANGUAGE']} as name
				    FROM tl_cpe_fahrzeuge 
				    INNER JOIN tl_cpe_fahrzeugarten on tl_cpe_fahrzeuge.fahrzeugart =  tl_cpe_fahrzeugarten.id
				");
                
               
		$tmpArray = array();
                $tmpArray[] = array ("label" => $GLOBALS['TL_LANG']['MOD']['ContaoPortalEngine']['alle'], "value" => 0);
		while ($rs->next())
		{
			$tmpArray[] = array ("label" => $rs->name, "value" => $rs->id);
		}
		$form1->options = serialize($tmpArray);
		$Template->fsmFahrzeugarten = $form1->generate(); 
                
                
                // fsmBaujahr
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selBaujahr";
                $form1->id = "selBaujahr";
                $form1->value = $_POST['selBaujahr'];
		
                
                /*
		$rs = $this->Database->query("
					SELECT distinct tl_cpe_fahrzeuge.fahrzeugart as id ,tl_cpe_fahrzeugarten.name_{$GLOBALS['TL_LANGUAGE']} as name
				    FROM tl_cpe_fahrzeuge 
				    INNER JOIN tl_cpe_fahrzeugarten on tl_cpe_fahrzeuge.fahrzeugart =  tl_cpe_fahrzeugarten.id
				");
                */
               
		$tmpArray = array();
                $tmpArray[] = array ("label" => $GLOBALS['TL_LANG']['MOD']['ContaoPortalEngine']['alle'], "value" => 0);
                
                for ($jahr = 1980; $jahr<=date("Y",time());$jahr++)
                {
                    $tmpArray[] = array ("label" => $jahr, "value" => $jahr);
                }
                
          
		$form1->options = serialize($tmpArray);
		$Template->fsmBaujahr = $form1->generate(); 
                
                
                // fsmAnzahlAchsen
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selAnzahlAchsen";
                $form1->id = "selAnzahlAchsen";
                $form1->value = $_POST['selAnzahlAchsen'];
		
                
                /*
		$rs = $this->Database->query("
					SELECT distinct tl_cpe_fahrzeuge.fahrzeugart as id ,tl_cpe_fahrzeugarten.name_{$GLOBALS['TL_LANGUAGE']} as name
				    FROM tl_cpe_fahrzeuge 
				    INNER JOIN tl_cpe_fahrzeugarten on tl_cpe_fahrzeuge.fahrzeugart =  tl_cpe_fahrzeugarten.id
				");
                */
               
		$tmpArray = array();
                $tmpArray[] = array ("label" => $GLOBALS['TL_LANG']['MOD']['ContaoPortalEngine']['alle'], "value" => 0);
                
                for ($anzAchsen = 3; $anzAchsen<=12;$anzAchsen++)
                {
                    $tmpArray[] = array ("label" => $anzAchsen, "value" => $anzAchsen);
                }
                
          
		$form1->options = serialize($tmpArray);
		$Template->fsmAnzahlAchsen= $form1->generate();
                
                
                
                /*
                
		// fsmSprachen
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selSprache";
		$form1->id = "selSprache";
		
		$rs = $this->Database->query("
							SELECT id ,sprache 
						    FROM tl_cpe_sprachen
						");
		
		$tmpArray = array();
		while ($rs->next())
		$tmpArray[] = array ("label" => $rs->sprache, "value" => $rs->id);
		
		$form1->options = serialize($tmpArray);
		$Template->fsmSprachen = $form1->generate();
		
		// fsmSchwerpunkte
		// ---------------------------------------------------------------------------
		$form1 = new FormSelectMenu();
		$form1->name = "selSchwerpunkt";
		$form1->id = "selSchwerpunkt";
		
		$rs = $this->Database->query("
									SELECT id ,schwerpunkt 
								    FROM tl_cpe_schwerpunkte
								");
		
		$tmpArray = array();
		while ($rs->next())
		$tmpArray[] = array ("label" => $rs->schwerpunkt, "value" => $rs->id);
		
		$form1->options = serialize($tmpArray);
		$Template->fsmSchwerpunkte = $form1->generate();
		
                */
		$Template->frmTarget = $GLOBALS['CPE-Conf']->URL_Modulordner ."/". $GLOBALS['CPE-Conf']->URL_fuer_Suche;
	
                
                //echo "y"; */
        }
	

	

	
}



?>
